export class ProductList{
  products = [
    {
      name: 'Master 1',
      img:'',
      text:"",
      cost: '$1'
    },
    {
      name: 'Master 2',
      img:'',
      text:"",
      cost: '$2'
    },
    {
      name: 'Master 3',
      img:'',
      text:"",
      cost: '$3'
    },
    {
      name: 'Master 4',
      img:'',
      text:"",
      cost: '$4'
    },
    {
      name: 'Master 5',
      img:'',
      text:"",
      cost: '$5'
    },
    {
      name: 'Master 6',
      img:'',
      text:"",
      cost: '$6'
    },
    {
      name: 'Master 7',
      img:'',
      text:"",
      cost: '$7'
    },
    {
      name: 'Master 8',
      img:'',
      text:"",
      cost: '$8'
    },
    {
      name: 'Master 9',
      img:'',
      text:"",
      cost: '$9'
    },
    {
      name: 'Master 10',
      img:'',
      text:"",
      cost: '$10'
    },
    {
      name: 'Master 11',
      img:'',
      text:"",
      cost: '$11'
    },
    {
      name: 'Master 12',
      img:'',
      text:"",
      cost: '$12'
    },
    {
      name: 'Master 13',
      img:'',
      text:"",
      cost: '$13'
    },
    {
      name: 'Master 14',
      img:'',
      text:"",
      cost: '$14'
    },
    {
      name: 'Master 15',
      img:'',
      text:"",
      cost: '$15'
    },
    {
      name: 'Master 16',
      img:'',
      text:"",
      cost: '$16'
    },
    {
      name: 'Master 77',
      img:'',
      text:"",
      cost: '$17'
    },
    {
      name: 'Master 67',
      img:'',
      text:"",
      cost: '$18'
    },
    {
      name: 'Master 78',
      img:'',
      text:"",
      cost: '$19'
    },
    {
      name: 'Master 99',
      img:'',
      text:"",
      cost: '$20'
    }
  ];

  productsCopy=this.products;
  numberList=5;
  pageSee=1;
  product=this.productsCopy.slice(this.pageSee-1,this.numberList)
  buyList:{name:string,cost:string,count:number}[]=[];
  currentPrice:number[]=[0];

  setHowSee(number:number){
    this.pageSee=number
    this.product=this.productsCopy.slice(this.numberList * (this.pageSee-1),this.numberList*this.pageSee)
    console.log(this.product)
  }
  buyItem(name:string,cost:string,count:number){

      if(this.buyList.some((el)=>el.name===name)){
        this.addOne(name,cost,count)

      }else{
      console.log(this.buyList.some((el)=>el.name===name));
        this.buyList.push({name,cost,count:1});
        this.currentPrice[0] +=  +cost.slice(1,cost.length);
        console.log(this.buyList)
      }
  }
  delete(name:string,cost:string,count:number){

    this.buyList=this.buyList.filter((el)=>el.name!==name);
    this.currentPrice[0] -= +cost.slice(1,cost.length)*count;
    console.log(cost)
    console.log(this.currentPrice)
  }
  filter(from:string,to:string){
    this.productsCopy=this.products.filter(el=> +el.cost.slice(1,el.cost.length) > +from && +el.cost.slice(1,el.cost.length) < +to)
    console.log(this.products)
  }
  addOne(name:string,cost:string,count:number){
    this.buyList.forEach((el,index)=>{
      if(el.name===name){
        el.count+=1
        this.currentPrice[0] +=  +cost.slice(1,cost.length);
      }
    })
    console.log(this.buyList)
  }
  minusOne(name:string,cost:string,count:number){
    this.buyList.forEach((el,index)=>{
      if(el.name===name&&el.count>1){
        el.count-=1;
        this.currentPrice[0] -=  +cost.slice(1,cost.length);
      }else if(el.name===name&&el.count===1){
        this.delete(name,cost,count)
      }
    })
    console.log(this.buyList)
  }
  pay(){
    this.buyList=[];
    this.currentPrice=[0];
    console.log(this.buyList)
    console.log(this.currentPrice)
  }
}
