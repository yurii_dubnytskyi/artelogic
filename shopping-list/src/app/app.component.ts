import { Component } from '@angular/core';
import {ProductList} from "./productlist.service"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[ProductList]
})
export class AppComponent {
  title = 'shopping-list';
}
