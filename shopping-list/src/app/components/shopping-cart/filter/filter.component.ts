import { Component, OnInit } from '@angular/core';
import {ProductList} from "../../../productlist.service";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  from='';
  to='';
  constructor(private ProductList:ProductList) { }

  ngOnInit(): void {
  }
  filterPrice(){
    this.ProductList.filter(this.from,this.to)
  }
  onUpdateFrom(event:Event) {
    this.from = (<HTMLInputElement>event.target).value
  }
  onUpdateTo(event:Event) {
    this.to = (<HTMLInputElement>event.target).value
  }

}
