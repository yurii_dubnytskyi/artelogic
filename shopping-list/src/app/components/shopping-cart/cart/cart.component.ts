import {Component, Input, OnInit,
  DoCheck,
  OnChanges,
  AfterContentInit,
  AfterContentChecked,
  AfterViewChecked,
  AfterViewInit} from '@angular/core';
import {ProductList} from "../../../productlist.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, DoCheck{
  buy: {name: string,cost:string,count:number}[]=[];
  price: number[]=[];

  constructor(private ProductList:ProductList) {

  }

  ngOnInit(): void {

    this.buy = this.ProductList.buyList;
    this.price = this.ProductList.currentPrice
    console.log(this.buy)
  }
  ngDoCheck() {
    this.buy = this.ProductList.buyList;
    this.price = this.ProductList.currentPrice
  }


  delete(name:string,cost:string,count:number){
    this.ProductList.delete(name,cost,count)
  }
  addOne(name:string,cost:string,count:number){
    this.ProductList.addOne(name,cost,count)
  }
  minusOne(name:string,cost:string,count:number){
    this.ProductList.minusOne(name,cost,count)
  }
  pay(){

  }


}
