import { Component, OnInit ,OnChanges} from '@angular/core';
import {ProductList} from "../../../productlist.service";


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit,OnChanges {

  products:{name: string, text: string,cost:string}[]=[];


  constructor(private ProductList:ProductList) {

  }
  ngOnChanges():void{

    this.products=this.ProductList.product
    console.log(this.products)
  }

  ngOnInit(): void {

    this.products=this.ProductList.product
    console.log(this.products)
  }


  onSetToPage(status: number) {
    this.ProductList.setHowSee(status)
    this.products=this.ProductList.product
  }

}
