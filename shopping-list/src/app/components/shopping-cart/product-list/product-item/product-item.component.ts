import {Component, Input, OnInit} from '@angular/core';
import {ProductList} from "../../../../productlist.service";

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  @Input() product: {name: string, text: string,cost:string,count:number};
  constructor(private ProductList:ProductList) { }

  ngOnInit(): void {
    console.log(this.product)
  }

  buyItem(name:string,cost:string,count:number){
    this.ProductList.buyItem(name,cost,count)
  }

}
