import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import {MainComponent} from "./main.component";
import {HeaderComponent} from "../header/header.component";
import {FooterComponent} from "../footer/footer.component";
import {ShoppingCartComponent} from "../shopping-cart/shopping-cart.component";
import {FilterComponent} from "../shopping-cart/filter/filter.component";
import {ProductListComponent} from "../shopping-cart/product-list/product-list.component";
import {ProductItemComponent} from "../shopping-cart/product-list/product-item/product-item.component";
import {CartComponent} from "../shopping-cart/cart/cart.component";



@NgModule({
  declarations: [MainComponent,HeaderComponent,
    FooterComponent,ShoppingCartComponent,FilterComponent,
    CartComponent,
    ProductListComponent,
    ProductItemComponent],
  imports: [
    CommonModule,
    MainRoutingModule
  ]
})
export class MainModule { }
