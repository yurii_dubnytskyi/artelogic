import { Component, OnInit,DoCheck } from '@angular/core';
import {ProductList} from "../../productlist.service";

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.css']
})
export class PayComponent implements OnInit,DoCheck {
  price: number[]=[];
  buy: {name: string,cost:string,count:number}[]=[];
  constructor(private ProductList:ProductList) { }

  ngOnInit(): void {
    this.price = this.ProductList.currentPrice
    this.buy = this.ProductList.buyList;
  }
  ngDoCheck() {
    this.price = this.ProductList.currentPrice
    this.buy = this.ProductList.buyList;
  }
  pay(){
    this.ProductList.pay()
  }

}
