import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from "./components/main/main.component";
import {PayComponent} from "./components/pay/pay.component";

const routes: Routes = [
  {
    path: 'shopping',
    component: MainComponent,
    data: { title: 'Heroes List' }
  },
  { path: '',
    redirectTo: '/shopping',
    pathMatch:"full"
  },
  { path: 'pay', component: PayComponent,pathMatch:"full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{enableTracing:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
