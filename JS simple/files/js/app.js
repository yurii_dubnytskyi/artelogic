let questions=[
    {   question:`<pre>Що виведе ???
let a = new Array(1,2), b = new Array(3);
alert(a[0] + b[0]);
                  </pre>`,
        variants:["1","NaN", "4", "undefined"],
        answer:1,
        userAnswer:undefined},

    {   question:`<pre>Який результат ???
null + {0:1}[0] + [,[1],][1][0]
                 </pre>`,
        variants:["0", "1", "NaN", "2"],
        answer:3,
        userAnswer:undefined},

    {   question:`<pre>Що виведе функція sayHi???
let name = \"Вася\";
function sayHi() {
alert(name);
}
      
setTimeout(function() {
let name = \"Вова\;
sayHi();
}, 1000);
                   </pre>`,
        variants:["Вася", "Вова", "undefined","Error"],
        answer:0,
        userAnswer:undefined},

    {   question:`<pre>Що виведе код ???
function User() { }
User.prototype = { admin: false };
let user = new User();
alert(user.admin);
                </pre>`,
        variants:["false", "true", "neither","undefined"],
        answer:0,
        userAnswer:undefined},

    {   question:`<pre>Який правильний синтаксис JavaScript змінює зміст елемента HTML нижче?
&lt;p id='demo' &gt;This is a demonstration.&lt;/p&gt;
    	          </pre>`,
        variants:["document.getElement(\"p\").innerHTML = \"Hello World!\";",
            "document.getElementById(\"demo\").innerHTML = \"Hello World!\";",
            "#demo.innerHTML = \"Hello World!\";",
            "document.getElementByName(\"p\").innerHTML = \"Hello World!\";"],
        answer:1,
        userAnswer:undefined},

    {   question:`<pre>Як написати IF-розгалуження в JavaScript?</pre>`,
        variants:["if (i === 5)", "if i = 5", "if i == 5 then","if i = 5 then"],
        answer:0,
        userAnswer:undefined},

    {   question:`<pre>Який цикл FOR правильний ???</pre>`,
        variants:["for i = 1 to 5", "for (i <= 5; i++)", "for (i = 0; i <= 5)","for (;;)"],
        answer:3,
        userAnswer:undefined},

    {   question:`<pre>Який правильний спосіб написати масив JavaScript?</pre>`,
        variants:["var colors = \"red\", \"green\", \"blue\"",
            "var colors = (1:\"red\", 2:\"green\", 3:\"blue\")",
            "var colors = [\"red\", \"green\", \"blue\"]",
            "var colors = 1 = (\"red\"), 2 = (\"green\"), 3 = (\"blue\")"
        ],
        answer:2,
        userAnswer:undefined},

    {   question:`<pre>Яка подія відбувається, коли користувач натискає на елемент HTML?</pre>`,
        variants:["onchange", "onclick", "onmouseclick","onmouseover"],
        answer:1,
        userAnswer:undefined},

    {   question:`<pre>Чи є JavaScript кросплатформним ???</pre>`,
        variants:["Yes", "No", "I don't know","maybe"],
        answer:0,
        userAnswer:undefined}

];

let clickIndexQuestions = 0;
let resultPoint=0;
let attempt=0;
const maxResult=10;
const listItems = document.querySelectorAll(".li");

const addItemAndEvent = () => {
    questions[clickIndexQuestions].variants.forEach( (el, index) => listItems[index].innerHTML = questions[clickIndexQuestions].variants[index] );

    listItems.forEach( el => {el.addEventListener('click', function(evert){
                                listItems.forEach(element => element.classList.remove('enable'));
                                this.classList.add("enable");
                                })
                        });
};

const start = () => {
    const block = document.getElementById("question-block");
    block.style.display="block";

    const startBlock = document.getElementById("start-block");
    startBlock.style.display="none";

    const question = document.getElementsByClassName("question");
    question[0].innerHTML=questions[clickIndexQuestions].question;

    addItemAndEvent();

    clickIndexQuestions++;
};

const submit = () => {
    for(let i=0;i<listItems.length;i++){
        if(listItems[i].classList.contains('enable')){
            questions[clickIndexQuestions-1].userAnswer=i;
            listItems[i].classList.remove("enable");
            break;
        }else if(i === listItems.length-1){
            alert("You must answer the question");
            return
        }
    }

    if(clickIndexQuestions === questions.length){
        clickIndexQuestions = 0;
        finish();
        return
    }

    listItems.forEach(element => element.innerHTML="");
    let question = document.getElementsByClassName('question');
    question[0].innerHTML=questions[clickIndexQuestions].question;

    questions[clickIndexQuestions].variants.forEach( (el, index) => { listItems[index].appendChild(document.createTextNode(questions[clickIndexQuestions].variants[index]))});

    clickIndexQuestions++
};

const finish = () => {
    const line = document.getElementsByClassName('line');
    let rotate;

    const block = document.getElementById('question-block');
    block.style.display='none';

    const finishBlock = document.getElementById('finish-block');
    finishBlock.style.display='block';

    const result = document.getElementsByClassName('interest');
    questions.forEach( (el, index) => {
        if(questions[index].answer === questions[index].userAnswer){
            resultPoint++
        }
    });
    rotate= resultPoint*18;
    result[0].innerHTML=(resultPoint*100)/maxResult+"%";
    line[0].style.transform="rotate("+rotate+"deg)";
    localStorage.setItem('Attempt'+attempt, ''+resultPoint);
    attempt++;
};

const again = () => {
    const fullBlock = document.getElementById('fullResult');
    fullBlock.innerHTML='';
    fullBlock.style.display='none';

    const block = document.getElementById('question-block');
    block.style.display='block';

    const finishBlock = document.getElementById('finish-block');
    finishBlock.style.display='none';

    const question = document.getElementsByClassName('question');
    question[0].innerHTML=questions[clickIndexQuestions].question;

    listItems.forEach(element => element.classList.remove('enable'));

    questions[clickIndexQuestions].variants.forEach( (el, index) => { listItems[index].appendChild(document.createTextNode(questions[clickIndexQuestions].variants[index]))});

    resultPoint=0;
    clickIndexQuestions++;
};

const result = () => {
    const fullBlock = document.getElementById('fullResult');
    fullBlock.style.display='block';
    fullBlock.innerHTML='';

    for(let i =0;i<questions.length;i++){
        const newDiv = document.createElement("div");
        const newHeader = document.createElement("h2");
        newHeader.innerHTML=questions[i].question;

        const newUl = document.createElement("ul");
        newUl.classList.add("list");

        for(let j=0;j<questions[i].variants.length;j++){
            const newLi = document.createElement("li");
            newLi.innerHTML=questions[i].variants[j];
            newLi.classList.add("li");
            if(j===questions[i].answer){
                newLi.classList.add('correct')
            }else if(j===questions[i].userAnswer){
                newLi.classList.add('wrong')
            }
            newUl.appendChild(newLi)
        }

        newDiv.appendChild(newHeader);
        newDiv.appendChild(newUl);
        fullBlock.appendChild(newDiv);
    }
};